package model;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CollectionPapillon {

    private ObservableList<Papillon> listPapillonObs = FXCollections.observableArrayList();
        private ListProperty<Papillon> listPapillon = new SimpleListProperty<>(listPapillonObs);
        public ObservableList<Papillon> getListPapillonObs() {return FXCollections.unmodifiableObservableList(listPapillon.get());}
        public ReadOnlyListProperty<Papillon> lesPapillonProperty(){ return listPapillon; }


    /**
     * add pap
     */
    public void addPapillon(String name){
        listPapillonObs.add(new Papillon(name));
    }


    /**
     * delete papillon
     */
    public void deletePapillon(Papillon pap){
        listPapillonObs.remove(pap);
    }
}
