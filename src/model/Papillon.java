package model;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Papillon {

    /**
     *Nom Papilon
     */
    private StringProperty nom = new SimpleStringProperty();
        public String getNom(){return nom.get();};
        public StringProperty nomProperty(){return nom;}
        public void setNom(String nom){this.nom.set(nom);}
    /**
     * obs wings colors
     */
    private ObservableList<Couleur> lesCouleursObs = FXCollections.observableArrayList();
        private ListProperty<Couleur> lesCouleurs = new SimpleListProperty<>(lesCouleursObs);
        public ObservableList<Couleur> getLesCouleurs(){return FXCollections.unmodifiableObservableList(lesCouleurs.get());}
        public ReadOnlyListProperty<Couleur> lesCouleuresProperty(){return lesCouleurs;}


    /**
     * constructor
     * @para nom
     */
    Papillon(String nom){
        setNom(nom);
    }

    /**
     * ajtColor
     */

    public void addColor(int r, int g, int b){
        lesCouleursObs.add(new Couleur(r,g,b));
    }

    /**
     * delete Color
     */
    public void deleteColor(Couleur clr){
        lesCouleursObs.remove(clr);
    }
}
