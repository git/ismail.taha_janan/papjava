package luncher;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MesPapillons extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/PrincipalView.fxml"));
        Scene sc = new Scene(root);
        stage.setTitle("lol");
        stage.setScene(sc);
        stage.show();
    }
}
